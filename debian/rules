#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

include /usr/share/dpkg/pkg-info.mk

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

export QT_SELECT := qt5

%:
	dh $@ --with pkgkde_symbolshelper --buildsystem=qmake

override_dh_auto_clean:
	dh_auto_clean
	rm -rfv doc/html tests/auto/*/testWorkDir lib
	rm -fv doc/qbs.qch bin/qbs* bin/tst*
	find . -type f -name .gitignore -delete -print

override_dh_auto_configure:
	dh_auto_configure -- qbs.pro \
		CONFIG+=debug \
		CONFIG+=qbs_disable_rpath \
		CONFIG+=qbs_enable_project_file_updates \
		CONFIG+=qbs_enable_unit_tests \
		QBS_INSTALL_PREFIX=/usr \
		QBS_LIBRARY_DIRNAME=lib/${DEB_HOST_MULTIARCH}

override_dh_auto_build-indep:
	dh_auto_build -- docs
	dh_auto_build -- sub-static-res-pro

override_dh_auto_install-indep:
	dh_auto_build -- INSTALL_ROOT=$(CURDIR)/debian/tmp install_inst_html_docs
	dh_auto_build -- INSTALL_ROOT=$(CURDIR)/debian/tmp sub-static-pro-install_subtargets
	dh_auto_build -- INSTALL_ROOT=$(CURDIR)/debian/tmp sub-static-res-pro-install_subtargets

override_dh_auto_test-arch:
ifeq ($(filter nocheck,$(DEB_BUILD_OPTIONS)),)
	set -ex; \
		export LD_LIBRARY_PATH=$(CURDIR)/lib/${DEB_HOST_MULTIARCH}:$$LD_LIBRARY_PATH; \
		export HOME=$(CURDIR)/debian/.debhelper/generated/_source/home; \
		export QBS_AUTOTEST_PROFILE=qbs_autotests; \
		export QBS_AUTOTEST_ALWAYS_LOG_STDOUT=1; \
		export QBS_AUTOTEST_ALWAYS_LOG_STDERR=1; \
		$(CURDIR)/bin/qbs-setup-toolchains /usr/bin/gcc gcc; \
		$(CURDIR)/bin/qbs-setup-qt /usr/bin/qmake qbs_autotests; \
		[ -n "`$(CURDIR)/bin/qbs-config --list profiles.qbs_autotests.baseProfile`" ] || \
			$(CURDIR)/bin/qbs-config profiles.qbs_autotests.baseProfile gcc; \
		LC_ALL=C.UTF-8 dh_auto_test --max-parallel=1
endif

override_dh_auto_test-indep:

override_dh_install:
	rm -fv debian/tmp/usr/lib/${DEB_HOST_MULTIARCH}/libqbscore.so.1
	rm -fv debian/tmp/usr/libexec/qbs/dmgbuild
	rm -rfv debian/tmp/usr/share/qbs/python
	dh_install
